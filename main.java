// Creater : KAPIL KUMAR
// EMAIL : THEJSBOY1@GMAIL.COM


import java.util.Random;

class Employee {
    String firstName;
    String lastName;
    String department;

    // Parameterized constructor
    public Employee(String firstName, String lastName, String department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
    }
}

class CredentialService {
    // Method to generate a random password
    static String generatePassword() {
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCase = "abcdefghijklmnopqrstuvwxyz";
        String digits = "0123456789";
        String specialCharacters = "!@#$%^&*()-_=+<>?[]{},.";
        String allCharacters = upperCase + lowerCase + digits + specialCharacters;

        StringBuilder password = new StringBuilder();
        Random random = new Random();

        password.append(upperCase.charAt(random.nextInt(upperCase.length())));
        password.append(lowerCase.charAt(random.nextInt(lowerCase.length())));
        password.append(digits.charAt(random.nextInt(digits.length())));
        password.append(specialCharacters.charAt(random.nextInt(specialCharacters.length())));

        for (int i = 0; i < 8; i++) {
            password.append(allCharacters.charAt(random.nextInt(allCharacters.length())));
        }

        // Shuffle the password characters
        char[] passwordArray = password.toString().toCharArray();
        for (int i = 0; i < passwordArray.length; i++) {
            int randomIndex = random.nextInt(passwordArray.length);
            char temp = passwordArray[i];
            passwordArray[i] = passwordArray[randomIndex];
            passwordArray[randomIndex] = temp;
        }

        return new String(passwordArray);
    }

    // Method to generate email address
    static String generateEmailAddress(Employee employee) {
        return employee.firstName.toLowerCase() + employee.lastName.toLowerCase() + "@" +
                employee.department.toLowerCase() + ".abc.com";
    }

    // Method to display generated credentials
    static void showCredentials(Employee employee, String password) {
        System.out.println("Dear " + employee.firstName + " " + employee.lastName+ " your generated credentials are as follows");
        System.out.println("Email ---> " + generateEmailAddress(employee));
        System.out.println("Password ---> " + password);
    }
}

// Dear Harshit your generated credentials are as follows
// Email ---> harshitchoudary@tech.abc.com
// Password ---> 181E@wFT

public class Main {
    public static void main(String[] args) {
        // Example of usage
        Employee newEmployee = new Employee("Harshit", "choudary", "Technical");

        // Using CredentialService to generate and display credentials
        String password = CredentialService.generatePassword();
        CredentialService.showCredentials(newEmployee, password);
    }
}
